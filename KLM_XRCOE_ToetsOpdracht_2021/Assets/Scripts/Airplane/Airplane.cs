using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Airplane", order = 1)]
public class Airplane : ScriptableObject
{
    public string aircraftType;
    public string airplaneBrand;
}
