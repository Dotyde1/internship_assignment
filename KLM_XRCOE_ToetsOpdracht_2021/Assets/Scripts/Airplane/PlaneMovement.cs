using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlaneMovement : MonoBehaviour
{
    public bool randomizedMovement = true;
    public NavMeshAgent agent;

    public float wanderRadius;
    public float wanderTimer;

    private Transform target;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        timer = wanderTimer;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= wanderTimer)
        {
            if (randomizedMovement)
            {
               agent.SetDestination(RandomMovement(transform.position, wanderRadius, -1));
                timer = 0;
            }
        }
    }

    public Vector3 RandomMovement(Vector3 origin, float distance, int layermask)
    {

        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * distance;

        randomDirection += origin;
        
        NavMesh.SamplePosition(randomDirection, out NavMeshHit navHit, distance, layermask);
        return navHit.position;
    }



}
