using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneSettings : MonoBehaviour
{

    public Airplane airplane;
    public int airplaneNumber;
    public string airplaneType;
    public string airplaneBrand;
    public GameObject assignedHangar;
    public bool headingToHangar = false;
    // Start is called before the first frame update
    void Start()
    {
        airplaneType = airplane.aircraftType;
        airplaneBrand = airplane.airplaneBrand;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
