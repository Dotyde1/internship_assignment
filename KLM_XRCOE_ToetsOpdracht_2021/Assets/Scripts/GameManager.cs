using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public GameObject[] airplaneHangars;
    public GameObject[] airPlanes;
    public float cameraSpeed = 1f;
    [SerializeField]
    private Camera Camera;
    [SerializeField]
    private GameObject gameWorld;


    // Start is called before the first frame update
    void Start()
    {

        // each hangar will be assigned a airplane.
        int i = 1;
        foreach (GameObject hangar in airplaneHangars)
        {
            HangarManagement hangarManagement = hangar.GetComponent<HangarManagement>();
            //hangars will always be given the same number. ( It woudn't make much sense if the hangers would be given a different number if a plane leaves or arrives at the airport)
            hangarManagement.hangarNumber = i;

            foreach (GameObject plane in airPlanes)
            {
                PlaneSettings airPlaneSettings = plane.GetComponent<PlaneSettings>();
                //no hangar will be assigned to 2 or more planes and vice versa
                if (airPlaneSettings.assignedHangar == null && hangarManagement.assignedAirplane == null)
                {
                    airPlaneSettings.assignedHangar = hangar;
                    airPlaneSettings.airplaneNumber = i;

                    hangarManagement.assignedAirplane = plane;
                    i++;
                }
            }
            // the assigned number will be given to a hangar
            hangarManagement.textView.GetComponent<TextMeshProUGUI>().text = hangarManagement.hangarNumber.ToString();
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Camera.transform.RotateAround(gameWorld.transform.position, Vector3.up, cameraSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            Camera.transform.RotateAround(gameWorld.transform.position, Vector3.up, -cameraSpeed * Time.deltaTime);
        }
    }

        // each plane will return to it's designated hangar
        public void ParkPlane()
    {
        foreach (GameObject plane in airPlanes)
        {
            NavMeshAgent agent = plane.GetComponent<NavMeshAgent>();

            plane.GetComponent<PlaneMovement>().randomizedMovement = false;
            agent.SetDestination(plane.GetComponent<PlaneSettings>().assignedHangar.transform.position);

            IEnumerator coroutine = CheckArrival(plane, agent, plane.GetComponent<PlaneSettings>().assignedHangar);
            StartCoroutine(coroutine);
        }
    }

    IEnumerator CheckArrival(GameObject plane, NavMeshAgent agent,GameObject assignedHangar)
    {
        float dist = agent.remainingDistance;
        while (dist > .1f)
        {
            dist = agent.remainingDistance;

            if (dist < .2f)
            {
                plane.GetComponent<PlaneSettings>().assignedHangar.GetComponent<HangarManagement>().textView.GetComponent<TextMeshProUGUI>().color = Color.green;
                yield return null;
            }
            yield return new WaitForSeconds(1);
        }
    }

    //will toggle the lights of every plane on or off
    public void ToggleLights()
    {
        foreach (GameObject plane in airPlanes)
        {
            PlaneFlash airPlaneFlash = plane.GetComponent<PlaneFlash>();

            if (plane.GetComponent<PlaneSettings>().assignedHangar != null)
            {
                airPlaneFlash.myLight.gameObject.SetActive(!airPlaneFlash.myLight.gameObject.gameObject.activeInHierarchy);
            }
        }
    }
}
